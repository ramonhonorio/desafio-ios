//
//  SearchResultSpec.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 14/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit

// Tests
import Quick
import Nimble

// JSON
import ObjectMapper

class SearchResultSpec: QuickSpec {
    
    override func spec() {
        var result: SearchResult?
        beforeEach {
            // loading searchResult.json
            if let jsonData = CSHelpers.loadJSON(forFilename: "searchResult") {
                result = SearchResult(JSON: jsonData)!
            }
        }
        
        describe("when loaded json", closure: {
            it("attribs total number in server", closure: {
                expect(result?.totalCount).to(equal(2701171)) // total_count from service
            })
            
            it("attribs number of returned items", closure: {
                expect(result?.items?.count).to(equal(30)) // default number of items from service
            })
        })
    }
    
}
