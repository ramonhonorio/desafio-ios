//
//  Desafio_iOSTests.swift
//  Desafio-iOSTests
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import XCTest
@testable import Desafio_iOS

class Desafio_iOSTests: XCTestCase {
    
    var searchReposVC: SearchReposTableViewController!

    override func setUp() {
        super.setUp()
        
        // Carregando a ViewController de search dos repositórios
        searchReposVC = UIStoryboard(name: "Main",
                                     bundle: Bundle(for: self.classForCoder)).instantiateViewController(withIdentifier: "searchReposVC") as! SearchReposTableViewController
        searchReposVC.preload()
        
        // Carrega a searchReposVC com as informações do JSON de teste
        self.loadJSONTestFile()
        
    }
    
    // O número de items adicionados inicialmente a tableview deve ser a mesma quantidade que veio serviço
    func testItemNumberInTableMustBeEqualToArray(){
        XCTAssertEqual(searchReposVC.tableView.numberOfRows(inSection: 0), 30, "@error: number of items in table must be 30.")
    }
    
    private func loadJSONTestFile() {
        if let jsonData = CSHelpers.loadJSON(forFilename: "searchResult") {
            guard let result = SearchResult(JSON: jsonData) else {
                XCTFail("JSON File not loaded")
                return
            }
            
            // Adicionando repositórios do JSON à tableView
            for resultItem in result.items! {
                let itemVM = RepositoryViewModel(repository: resultItem)
                searchReposVC.items.append(itemVM)
            }
        }
    }
    
}

extension UIViewController {
    func preload() {
        _ = self.view
    }
}
