//
//  SearchReposTableViewController.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit

class SearchReposTableViewController: UITableViewController {
    
    var currentPage = 1
    
    var searchResult: SearchResult?
    
    var items = [RepositoryViewModel]()
    
    var selectedRepo: RepositoryViewModel?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getNextRepositoriesPage()
        
        // ActivityIndicator não aparece quando não está sendo animado
        self.activityIndicator.hidesWhenStopped = true
        
        // Removendo "Back" do botão de voltar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }

    // MARK: - TableView data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as! RepositoryTableViewCell
        
        let repository = items[indexPath.item]
        cell.fill(with: repository)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRepo = items[indexPath.item]
        self.performSegue(withIdentifier: "pullDetail", sender: self)
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = items.count - 1
        if indexPath.row == lastElement {
            
            if items.count < (searchResult?.totalCount ?? 0) {
                self.getNextRepositoriesPage()
            }
        }
    }
    
    // MARK: - Storyboard functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pullDetail" {
            
            let destination = segue.destination as! PullRequestsViewController
            destination.repoPullsURL = self.selectedRepo?.pullsURLString
            destination.repoTitle = self.selectedRepo?.name
        }
    }
    
    // MARK: - Helper functions
    func prepareItemViewModel() {
        guard let itemResultList = searchResult?.items else {
            print(">> Erro ao tentar apresentar resultados.")
            return
        }
        
        for resultItem in itemResultList {
            let itemVM = RepositoryViewModel(repository: resultItem)
            self.items.append(itemVM)
        }
        
        self.tableView.reloadData()
    }
    
    func getNextRepositoriesPage() {
        if self.items.count < (searchResult?.totalCount ?? 0) {
            currentPage += 1
        }
        
        self.activityIndicator.startAnimating()
        
        GithubClient.searchRepositories(page: currentPage, completion: { result in
            guard let result = result else {
                self.searchResult = nil
                return
            }
            self.searchResult = result
            self.prepareItemViewModel()
            self.activityIndicator.stopAnimating()
        })
    }
}
