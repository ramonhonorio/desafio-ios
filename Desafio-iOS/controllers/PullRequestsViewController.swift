//
//  PullRequestsViewController.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var repoPullsURL: String!
    var repoTitle: String!
    
    var searchResultArray: [PullRequest]?
    
    var items = [PullRequestViewModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = repoTitle
        
        // Do any additional setup after loading the view.
        GithubClient.searchPullRequests(url: URL(string: repoPullsURL)!) { (resultArray) in
            guard let arrayPullRequests = resultArray else{
                return
            }
            self.searchResultArray = arrayPullRequests
            self.prepareItemViewModel()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequestURL = items[indexPath.item].htmlUrl
        UIApplication.shared.open(URL(string: pullRequestURL)!, options: [:], completionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullRequestCell", for: indexPath) as! PullRequestTableViewCell
        
        let pullReq = items[indexPath.item]
        cell.fill(with: pullReq)
        
        return cell
    }
    
    // MARK: - Helper functions
    func prepareItemViewModel() {
        guard let itemResultList = searchResultArray else {
            print(">> Erro ao tentar apresentar resultados.")
            return
        }
        
        for pullReq in itemResultList {
            self.items.append(PullRequestViewModel(pullRequest: pullReq))
        }
        
        self.tableView.reloadData()
    }
    
}
