//
//  GithubClient.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

// MARK: - Handlers
typealias SearchHandler = (_ result:SearchResult?) -> Void
typealias UserHandler = (_ result:Owner?) -> Void
typealias PullReqHandler = (_ result:[PullRequest]?) -> Void

class GithubClient {
    
    static let baseSearchURL = "https://api.github.com/search/repositories?q=language:Java&sort=stars"
    
    // MARK: - GET Requests
    static func searchRepositories(page:Int, completion:@escaping SearchHandler) {
        let searchURL = "\(baseSearchURL)&page=\(page)"
        
        Alamofire.request(searchURL).responseObject { (response: DataResponse<SearchResult>) in
            completion(response.result.value)
        }
    }
    
    static func searchUser(url: URL, completion:@escaping UserHandler) {
        Alamofire.request(url).responseObject { (response: DataResponse<Owner>) in
            completion(response.result.value)
        }
    }
    
    static func searchPullRequests(url: URL, completion:@escaping PullReqHandler) {
        Alamofire.request(url).responseArray { (response: DataResponse<[PullRequest]>) in
            completion(response.result.value)
        }
    }
    
}
