//
//  RepositoryTableViewCell.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit
import Imaginary

class RepositoryTableViewCell: UITableViewCell {
    
    // MARK: Repository informations
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    
    // MARK: Owner informations
    @IBOutlet weak var ownerAvatarImage: UIImageView!
    @IBOutlet weak var ownerLoginLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    // MARK: Miscellanous
    @IBOutlet weak var backgroundCard: MaterialCard!
    
    var repo: RepositoryViewModel!
    
    func fill(with repo: RepositoryViewModel) {
        self.repo = repo
        
        nameLabel.text = repo.name
        descriptionLabel.text = repo.description
        forksLabel.text = repo.forks
        starsLabel.text = repo.stars
        
        guard let owner = repo.owner else {
            return
        }
        ownerLoginLabel.text = owner.login
        
        // Faz a requisição para o nome completo
        if owner.name.isEmpty {
            GithubClient.searchUser(url: owner.url!) { (ownerFullInfo) in
                self.ownerNameLabel.text = ownerFullInfo?.name ?? ""
            }
        }
        
        // Carrega a imagem do avatar
        ownerAvatarImage.setImage(url: owner.avatarUrl)
        CSHelpers.makeRounded(imageView: ownerAvatarImage)
        
        backgroundCard.cornerRadius = 10.0
    }

}
