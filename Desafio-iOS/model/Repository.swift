//
//  Item.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var id: Int?
    var name: String?
    var fullName: String?
    var description: String?
    var stars: Int?
    var forks: Int?
    var pullsUrl: String?
    
    var owner: Owner?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        fullName <- map["full_name"]
        stars <- map["stargazers_count"]
        forks <- map["forks_count"]
        pullsUrl <- map["pulls_url"]
        
        owner <- map["owner"]
    }
    
}

class RepositoryViewModel {
    private var repo: Repository
    
    init(repository: Repository) {
        self.repo = repository
    }
    
    var id:Int {
        return repo.id ?? -1
    }
    
    var description:String {
        return repo.description ?? ""
    }
    
    var name:String {
        return repo.name ?? ""
    }
    
    var fullname:String {
        return repo.fullName ?? ""
    }
    
    var stars:String {
        return repo.stars?.description ?? "0"
    }
    
    var forks:String {
        return repo.forks?.description ?? "0"
    }
    
    var pullsURLString:String? {
        return repo.pullsUrl?.replacingOccurrences(of: "{/number}", with: "") ?? ""
    }
    
    var owner:OwnerViewModel? {
        guard let owner = repo.owner else {
            return nil
        }
        return OwnerViewModel(owner: owner)
    }
}
