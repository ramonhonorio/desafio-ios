//
//  PullRequest.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: Mappable {
    
    var id: Int?
    var title: String?
    var body: String?
    var htmlUrl: String?
    var createdAt: String?
    
    var owner: Owner?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
        htmlUrl <- map["html_url"]
        createdAt <- map["created_at"]
        owner <- map["user"]
    }
    
}

class PullRequestViewModel {
    private var pullRequest: PullRequest
    
    init(pullRequest: PullRequest) {
        self.pullRequest = pullRequest
    }
    
    var id:Int {
        return pullRequest.id ?? -1
    }
    
    var title:String {
        return pullRequest.title ?? ""
    }
    
    var body:String {
        return pullRequest.body ?? ""
    }
    
    var htmlUrl:String {
        return pullRequest.htmlUrl ?? ""
    }
    
    var createdAt:Date {
        return CSHelperDate.timestampFormatter.date(from: pullRequest.createdAt!)!
    }
    
    var owner:OwnerViewModel? {
        guard let owner = pullRequest.owner else {
            return nil
        }
        return OwnerViewModel(owner: owner)
    }

}
