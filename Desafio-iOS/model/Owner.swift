//
//  Owner.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit
import ObjectMapper

class Owner: Mappable {
    
    var id: Int?
    var login: String?
    var name: String?
    var avatarUrl: String?
    var url: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        name <- map["name"]
        avatarUrl <- map["avatar_url"]
        url <- map["url"]
    }
    
}

class OwnerViewModel {
    private var owner: Owner
    
    init(owner: Owner) {
        self.owner = owner
    }
    
    var id:Int {
        return owner.id ?? -1
    }
    
    var login:String {
        return owner.login ?? ""
    }
    
    var name:String {
        return owner.name ?? ""
    }
    
    var url:URL? {
        return URL(string: owner.url ?? "")
    }
    
    var avatarUrl:URL? {
        return URL(string: owner.avatarUrl ?? "")
    }
}
