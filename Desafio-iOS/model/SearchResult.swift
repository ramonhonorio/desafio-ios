//
//  SearchResult.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchResult: Mappable {
    
    var totalCount: Int?
    var incompleteResults: String?
    var items: [Repository]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        totalCount <- map["total_count"]
        incompleteResults <- map["incomplete_results"]
        items <- map["items"]
    }
    
}
