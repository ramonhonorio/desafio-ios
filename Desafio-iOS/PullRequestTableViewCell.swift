//
//  PullRequestTableViewCell.swift
//  Desafio-iOS
//
//  Created by Ramon Honorio on 12/01/17.
//  Copyright © 2017 ramonilho. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    
    // MARK: - Pull Request Information
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!

    // MARK: - Owner Information
    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var userLoginLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    // MARK: - Miscellanous
    @IBOutlet weak var backgroundCard: MaterialCard!
    
    
    func fill(with pullReq:PullRequestViewModel) {
        titleLabel.text = pullReq.title
        bodyLabel.text = pullReq.body
        
        // Data de criação no formato BR (dd/MM/yyyy)
        createdAtLabel.text = CSHelperDate.formatterBR.string(from: pullReq.createdAt)
        
        guard let owner = pullReq.owner else {
            return
        }
        userLoginLabel.text = owner.login
        
        // Faz a requisição para o nome completo
        if owner.name.isEmpty {
            GithubClient.searchUser(url: owner.url!) { (ownerFullInfo) in
                self.userNameLabel.text = ownerFullInfo?.name ?? ""
            }
        }
        
        // Carrega a imagem do avatar
        userAvatarImage.setImage(url: owner.avatarUrl)
        CSHelpers.makeRounded(imageView: userAvatarImage)
        
        backgroundCard.cornerRadius = 10.0
    }

}
